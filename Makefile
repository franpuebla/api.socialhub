help:
	@echo "Shaas-k8s"
	@echo "Docker"
	@echo "make build"
	@echo "make run"
	@echo "make kill"
	@echo ""
	@echo "Kubernetes"
	@echo "k8s-get"
	@echo "k8s-init"
	@echo "k8s-apply"
	@echo "k8s-delete"
	@echo "k8s-import"
	@echo "k8s-shell"

src:
	rm -rf ./src/dist
	mkdir ./src/dist
	cd ./src
	gulp build

build:
	docker build --rm -t $(TAG) .
run:
	docker run -d --name $(TAG) --env-file $(ENV) -p $(LPORT):$(DPORT) $(TAG)
kill:
	docker kill $(TAG)
	docker rm $(TAG)

k8s-apply:
	$(KBCTL) apply -f .

k8s-shell:
	$(KBCTL) exec -it $(K8STAG)-0 ash

k8s-delete:
	$(KBCTL) delete -f .

k8s-get:
	$(KBCTL) get -f .

LPORT=5000
DPORT=80
TAG=api-socialhub
ENV=./Environment
KBCTL=kubectl --insecure-skip-tls-verify
K8STAG=api-socialhub
SHELL := /bin/bash
include $(ENV)
