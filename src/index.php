<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require __DIR__.'vendor/autoload.php';

$app = new \Slim\App;
\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
$ig = new \InstagramAPI\Instagram();

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});


// CORS
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});
$app->post('/config', function (Request $request, Response $response, array $args) use ($ig) {

    $parsedBody = $request->getParsedBody();
    $username = $parsedBody['username'];
    $password = $parsedBody['password'];
    //print_r($username);
    //die();

    $loginResponse = $ig->login($username, $password);

    // print_r($loginResponse);
    // die();
    $hashtag = $parsedBody['hashtag'];

    $results = $ig->hashtag->getStory($hashtag);
    $json_results = json_encode($results);

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->write($json_results);

    return $response;
});

$app->post('/inbox', function (Request $request, Response $response, array $args) use ($ig) {
    $parsedBody = $request->getParsedBody();
    $username = $parsedBody['username'];
    $password = $parsedBody['password'];
    //print_r($username);
    //die();

    $loginResponse = $ig->login($username, $password);

    $stories = [];
    //$hashtag = $args['hashtag'];
    $results = $ig->direct->getInbox();
    print_r ("resultadooo");
    print_r($results);
    $jsonS = json_decode($results);
    $pending = $jsonS->pending_requests_total;

    // $json_results = json_encode($pending);
    // $response = $response->withHeader('Content-Type', 'application/json');
    // $response->write($json_results);

    if ($pending != 0) {
        $results_pending = $ig->direct->getPendingInbox();
        $jsonS_pending = json_decode($results_pending);
        //print_r($jsonS_pending->inbox->threads);

        foreach ($jsonS_pending->inbox->threads as $valor) {
            $itemType = $valor->items[0]->item_type;
            if ($itemType == 'reel_share') {
                $expiring = $valor->items[0]->reel_share->media->expiring_at;
                if ($expiring != '0') {
                    $stories[] = $valor;
                }
            }
        }
    }

    foreach ($jsonS->inbox->threads as $valor) {
        $itemType = $valor->items[0]->item_type;
        if ($itemType == 'reel_share') {
            $expiring = $valor->items[0]->reel_share->media->expiring_at;
            if ($expiring != '0') {
                $stories[] = $valor;
            }
        }
    }
    $json_results = json_encode($stories);

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->write($json_results);

    return $response;
});

$app->post('/friendship', function (Request $request, Response $response, array $args) use ($ig) {
    $parsedBody = $request->getParsedBody();

    $username = $parsedBody['username'];
    $password = $parsedBody['password'];
    //print_r($username);
    //die();

    $loginResponse = $ig->login($username, $password);

    $userId = $ig->people->getUserIdForName('franpuebla');

    $results = $ig->people->getFriendship(198416083);
    $json_results = json_encode($results);

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->write($json_results);

    return $response;
});


$app->get('/hashtag/{hashtag}', function (Request $request, Response $response, array $args) use ($ig) {
    $hashtag = $args['hashtag'];

    $results = $ig->hashtag->getStory($hashtag);
    $json_results = json_encode($results);

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->write($json_results);

    return $response;
});

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});
$app->run();
