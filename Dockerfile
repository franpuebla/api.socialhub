FROM alpine:3.6
LABEL Maintainer="Fran Puebla <pueblafran@gmail.com>" \
      Description="Lightweight container with Nginx 1.12 & PHP-FPM 7.1 based on Alpine Linux. Adapted to use a docker-compose file and my own choice of packages - Big thanks to TrafeX/docker-php-nginx"

USER 0:0
# Install packages
RUN apk --no-cache add \
    curl \
    nginx \
    php7 \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-fpm \
    php7-gd \
    php7-json \
    php7-intl \
    php7-mbstring \
    php7-mysqli \
    php7-openssl \
    php7-phar \
    php7-xml \
    php7-xmlreader \
    php7-zlib \
    php7-exif \
    supervisor

# Create webroot directories
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/zzz_custom.conf
COPY config/php.ini /etc/php7/conf.d/zzz_custom.ini
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY src /var/www/html

RUN chmod -R 777 ./vendor

EXPOSE 80
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
